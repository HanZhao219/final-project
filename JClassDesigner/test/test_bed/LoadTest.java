/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;

import jcd.data.Argument;
import jcd.data.DataManager;
import jcd.data.DesignerClassRectangle;
import jcd.data.DesignerData;
import jcd.data.DesignerInterfaceRectangle;
import jcd.data.Enums.ProtectionType;
import jcd.data.Enums.StateType;
import jcd.data.Method;
import jcd.data.Variable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Han
 */
public class LoadTest {
    
    public LoadTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of saveModifications method, of class ThreadExample.
     */
    @Test
    public void testLoadModifications1() throws Exception {
        System.out.println("Test 1 \n");
        ThreadExample instance = new ThreadExample();
        instance.createScenarioOne();
        instance.saveModifications();
        instance.loadModifications();
        
        DataManager data = instance.getDataManager();
       
        DesignerData jClassDesigner = data.getData().get(0);
        
        assertTrue(jClassDesigner instanceof DesignerClassRectangle);
        
        System.out.println("\nPrinting Rectangle Stats: ");
        System.out.println("X Coordinate: " + jClassDesigner.getX() + " expected: 500");
        assertTrue(500 == jClassDesigner.getX());
        System.out.println("Y Coordinate: " + jClassDesigner.getY() + " expected: 250");
        assertTrue(250 == jClassDesigner.getY());
        System.out.println("Height: " + jClassDesigner.getHeight() + " expected: 300");
        assertTrue(300 == jClassDesigner.getHeight());
        System.out.println("Width: " + jClassDesigner.getWidth() + " expected: 200");
        assertTrue(200 == jClassDesigner.getWidth());
        
        System.out.println("Coordinate for first connector: (" + 
                jClassDesigner.getConnecterX1() + "," +
                jClassDesigner.getConnecterY1() + ") expected: (-1.0, -1.0)");
        assertTrue(-1 == jClassDesigner.getConnecterX1());
        assertTrue(-1 == jClassDesigner.getConnecterX2());
        System.out.println("Coordinate for second connector: (" + 
                jClassDesigner.getConnecterX2() + "," +
                jClassDesigner.getConnecterY2() + ") expected: (-1.0, -1.0)");
        assertTrue(-1 == jClassDesigner.getConnecterY1());
        assertTrue(-1 == jClassDesigner.getConnecterY2());
        
        System.out.println("\nPrinting Class Information: ");
        System.out.println("Class Name: " + jClassDesigner.getClassName() + " expected: JClassDesigner");
        assertEquals("JClassDesigner", jClassDesigner.getClassName());
        System.out.println("Package Name: " + jClassDesigner.getPackageName() + " expected: jcd");
        assertEquals("jcd", jClassDesigner.getPackageName());
        System.out.println("State: " + jClassDesigner.getState().name() + " expected: NONE");
        assertEquals(StateType.NONE, jClassDesigner.getState());
        System.out.println("Protection: " + jClassDesigner.getProtection().name() + " expected: PUBLIC");
        assertEquals(ProtectionType.PUBLIC, jClassDesigner.getProtection());
        
        
        System.out.println("\nPrinting Method Information: ");
        Method mainMethod = jClassDesigner.getMethods().get(0);
        System.out.println("Name: " + mainMethod.getName() + " expected: main");
        assertEquals("main", mainMethod.getName());
        System.out.println("State: " + mainMethod.getState().name() + " expected: STATIC");
        assertEquals(StateType.STATIC, mainMethod.getState());
        System.out.println("Protection: " + mainMethod.getProtection().name() + " expected: PUBLIC");
        assertEquals(ProtectionType.PUBLIC, mainMethod.getProtection());
        System.out.println("Return Type: " + mainMethod.getReturnType() + " expected: void");
        assertEquals("void", mainMethod.getReturnType());
        Argument arg1 = mainMethod.getArgs().get(0);
        System.out.println("Arguments: " + arg1.getName() + " expected: args");
        assertEquals("args", arg1.getName());
        System.out.println("Type: " + arg1.getType() + " expected: String[]");
        assertEquals("String[]", arg1.getType());
        Argument arg2 = mainMethod.getArgs().get(1);
        System.out.println("Arguments: " + arg2.getName() + " expected: idk");
        assertEquals("idk", arg2.getName());
        System.out.println("Type: " + arg2.getType() + " expected: int");
        assertEquals("int", arg2.getType());
        
        
        
        DesignerData workspaceClass = data.getData().get(1);
        assertTrue(workspaceClass instanceof DesignerClassRectangle);
        
        System.out.println("\n\nPrinting Rectangle Stats: ");
        System.out.println("X Coordinate: " + workspaceClass.getX() + " expected: 100");
        assertTrue(100 == workspaceClass.getX());
        System.out.println("Y Coordinate: " + workspaceClass.getY() + " expected: 100");
        assertTrue(100 == workspaceClass.getY());
        System.out.println("Height: " + workspaceClass.getHeight() + " expected: 300");
        assertTrue(300 == workspaceClass.getHeight());
        System.out.println("Width: " + workspaceClass.getWidth() + " expected: 200");
        assertTrue(200 == workspaceClass.getWidth());
        
        System.out.println("Coordinate for first connector: (" + 
                workspaceClass.getConnecterX1() + "," +
                workspaceClass.getConnecterY1() + ") expected: (-1.0, -1.0)");
        assertTrue(-1 == workspaceClass.getConnecterX1());
        assertTrue(-1 == workspaceClass.getConnecterX2());
        System.out.println("Coordinate for second connector: (" + 
                workspaceClass.getConnecterX2() + "," +
                workspaceClass.getConnecterY2() + ") expected: (-1.0, -1.0)");
        assertTrue(-1 == workspaceClass.getConnecterY1());
        assertTrue(-1 == workspaceClass.getConnecterY2());
        
        System.out.println("\nPrinting Class Information: ");
        System.out.println("Class Name: " + workspaceClass.getClassName() + " expected: Workspace");
        assertEquals("Workspace", workspaceClass.getClassName());
        System.out.println("Package Name: " + workspaceClass.getPackageName() + " expected: jcd.workspace");
        assertEquals("jcd.workspace", workspaceClass.getPackageName());
        System.out.println("State: " + workspaceClass.getState().name() + " expected: NONE");
        assertEquals(StateType.NONE, workspaceClass.getState());
        System.out.println("Protection: " + workspaceClass.getProtection().name() + " expected: PUBLIC");
        assertEquals(ProtectionType.PUBLIC, workspaceClass.getProtection());
        
        System.out.println("\nPrinting Variable Information: ");
        Variable canvasPaneVariable = workspaceClass.getVariables().get(0);
        System.out.println("Name: " + canvasPaneVariable.getName() + " expected: canvasPane");
        System.out.println("State: " + canvasPaneVariable.getState().name() + " expected: NONE");
        assertEquals(StateType.NONE, canvasPaneVariable.getState());
        System.out.println("Protection: " + canvasPaneVariable.getProtection().name() + " expected: PUBLIC");
        assertEquals(ProtectionType.PUBLIC, canvasPaneVariable.getProtection());
        
        System.out.println("\nPrinting Method Information: ");
        Method initToolbarMethod = workspaceClass.getMethods().get(0);
        System.out.println("Name: " + initToolbarMethod.getName() + " expected: initToolbar");
        assertEquals("initToolbar", initToolbarMethod.getName());
        System.out.println("State: " + initToolbarMethod.getState().name() + " expected: NONE");
        assertEquals(StateType.NONE, initToolbarMethod.getState());
        System.out.println("Protection: " + initToolbarMethod.getProtection().name() + " expected: PUBLIC");
        assertEquals(ProtectionType.PUBLIC, initToolbarMethod.getProtection());
        System.out.println("Return Type: " + initToolbarMethod.getReturnType() + " expected: void");
        assertEquals("void", initToolbarMethod.getReturnType());
        
        System.out.println("\nTest 1 Complete");
    }
    
    @Test
    public void testLoadModifications2() throws Exception {
        System.out.println("Test 2, Abstract Class start \n");
        ThreadExample instance = new ThreadExample();
        instance.createScenarioTwo();
        instance.saveModifications();
        instance.loadModifications();
        
        DataManager data = instance.getDataManager();
       
        DesignerData abstractClass = data.getData().get(2);
        
        assertTrue(abstractClass instanceof DesignerClassRectangle);
        
        System.out.println("\nPrinting Rectangle Stats: ");
        System.out.println("X Coordinate: " + abstractClass.getX() + " expected: 400");
        assertTrue(400 == abstractClass.getX());
        System.out.println("Y Coordinate: " + abstractClass.getY() + " expected: 200");
        assertTrue(200 == abstractClass.getY());
        System.out.println("Height: " + abstractClass.getHeight() + " expected: 300");
        assertTrue(300 == abstractClass.getHeight());
        System.out.println("Width: " + abstractClass.getWidth() + " expected: 200");
        assertTrue(200 == abstractClass.getWidth());
        System.out.println("Coordinate for first connector: (" + 
                abstractClass.getConnecterX1() + "," +
                abstractClass.getConnecterY1() + ") expected: (400.0, 160.0)");
        assertTrue(400 == abstractClass.getConnecterX1());
        assertTrue(160 == abstractClass.getConnecterY1());
        System.out.println("Coordinate for second connector: (" + 
                abstractClass.getConnecterX2() + "," +
                abstractClass.getConnecterY2() + ") expected: (200.0, 104.0)");
        assertTrue(200 == abstractClass.getConnecterX2());
        assertTrue(104 == abstractClass.getConnecterY2());
        
        System.out.println("\nPrinting Class Information: ");
        System.out.println("Class Name: " + abstractClass.getClassName() + " expected: SampleAbstractClass");
        assertEquals("SampleAbstractClass", abstractClass.getClassName());
        System.out.println("Package Name: " + abstractClass.getPackageName() + " expected: samp");
        assertEquals("samp", abstractClass.getPackageName());
        System.out.println("State: " + abstractClass.getState().name() + " expected: ABSTRACT");
        assertEquals(StateType.ABSTRACT, abstractClass.getState());
        System.out.println("Protection: " + abstractClass.getProtection().name() + " expected: PUBLIC");
        assertEquals(ProtectionType.PUBLIC, abstractClass.getProtection());
        
        System.out.println("\nPrinting Variable Information: ");
        Variable v1 = abstractClass.getVariables().get(0);
        System.out.println("Name: " + v1.getName() + " expected: x");
        assertEquals("x", v1.getName());
        System.out.println("Type: " + v1.getType() + " expected: int");
        assertEquals("int", v1.getType());
        Variable v2 = abstractClass.getVariables().get(1);
        System.out.println("Name: " + v2.getName() + " expected: y");
        assertEquals("y", v2.getName());
        System.out.println("Type: " + v2.getType() + " expected: int");
        assertEquals("int", v2.getType());
        
        System.out.println("\nPrinting Method Information: ");
        Method mainMethod = abstractClass.getMethods().get(0);
        System.out.println("Name: " + mainMethod.getName() + " expected: moveTo");
        assertEquals("moveTo", mainMethod.getName());
        System.out.println("Return Type: " + mainMethod.getReturnType() + " expected: void");
        assertEquals("void", mainMethod.getReturnType());
        
        Argument arg1 = mainMethod.getArgs().get(0);
        System.out.println("Arguments: " + arg1.getName() + " expected: newX");
        assertEquals("newX", arg1.getName());
        System.out.println("Type: " + arg1.getType() + " expected: int");
        assertEquals("int", arg1.getType());
        
        Argument arg2 = mainMethod.getArgs().get(1);
        System.out.println("Arguments: " + arg2.getName() + " expected: newY");
        assertEquals("newY", arg2.getName());
        System.out.println("Type: " + arg2.getType() + " expected: int");
        assertEquals("int", arg2.getType());
        
        
        Method m2 = abstractClass.getMethods().get(1);
        System.out.println("Name: " + m2.getName() + " expected: draw");
        assertEquals("draw", m2.getName());
        System.out.println("Return Type: " + m2.getReturnType() + " expected: void");
        assertEquals("void", m2.getReturnType());
        System.out.println("Return Type: " + m2.getState().name() + " expected: ABSTRACT");
        assertEquals(StateType.ABSTRACT, m2.getState());
        System.out.println("\nTest 2 complete");
    }
    
    @Test
    public void testLoadModifications3() throws Exception {
        System.out.println("Test 3, Interfaces start \n");
        ThreadExample instance = new ThreadExample();
        instance.createScenarioThree();
        instance.saveModifications();
        instance.loadModifications();
        
        DataManager data = instance.getDataManager();
       
        DesignerData interfaceSample = data.getData().get(3);
        
        assertTrue(interfaceSample instanceof DesignerInterfaceRectangle);
        
        System.out.println("\nPrinting Rectangle Stats: ");
        System.out.println("X Coordinate: " + interfaceSample.getX() + " expected: 100");
        assertTrue(100 == interfaceSample.getX());
        System.out.println("Y Coordinate: " + interfaceSample.getY() + " expected: 600");
        assertTrue(600 == interfaceSample.getY());
        System.out.println("Height: " + interfaceSample.getHeight() + " expected: 300");
        assertTrue(300 == interfaceSample.getHeight());
        System.out.println("Width: " + interfaceSample.getWidth() + " expected: 200");
        assertTrue(200 == interfaceSample.getWidth());
        System.out.println("Coordinate for first connector: (" + 
                interfaceSample.getConnecterX1() + "," +
                interfaceSample.getConnecterY1() + ") expected: (1237.0, 123.0)");
        assertTrue(1237 == interfaceSample.getConnecterX1());
        assertTrue(123 == interfaceSample.getConnecterY1());
        System.out.println("Coordinate for second connector: (" + 
                interfaceSample.getConnecterX2() + "," +
                interfaceSample.getConnecterY2() + ") expected: (663.0, 127.0)");
        assertTrue(663 == interfaceSample.getConnecterX2());
        assertTrue(127 == interfaceSample.getConnecterY2());
        
        
        System.out.println("\nPrinting Interface Information: ");
        System.out.println("Interface Name: " + interfaceSample.getClassName() + " expected: SampleInterfaceClass");
        assertEquals("SampleInterfaceClass", interfaceSample.getClassName());
        System.out.println("Package Name: " + interfaceSample.getPackageName() + " expected: samp");
        assertEquals("samp", interfaceSample.getPackageName());
        
        System.out.println("\nPrinting Variable Information: ");
        Variable v1 = interfaceSample.getVariables().get(0);
        System.out.println("Name: " + v1.getName() + " expected: speed");
        assertEquals("speed", v1.getName());
        System.out.println("Type: " + v1.getType() + " expected: int");
        assertEquals("int", v1.getType());
        Variable v2 = interfaceSample.getVariables().get(1);
        System.out.println("Name: " + v2.getName() + " expected: name");
        assertEquals("name", v2.getName());
        System.out.println("Type: " + v2.getType() + " expected: String");
        assertEquals("String", v2.getType());
        
        System.out.println("\nPrinting Method Information: ");
        Method mainMethod = interfaceSample.getMethods().get(0);
        System.out.println("Name: " + mainMethod.getName() + " expected: changeName");
        assertEquals("changeName", mainMethod.getName());
        System.out.println("Return Type: " + mainMethod.getReturnType() + " expected: void");
        assertEquals("void", mainMethod.getReturnType());
        
        Argument arg1 = mainMethod.getArgs().get(0);
        System.out.println("Arguments: " + arg1.getName() + " expected: newValue");
        assertEquals("newValue", arg1.getName());
        System.out.println("Type: " + arg1.getType() + " expected: String");
        assertEquals("String", arg1.getType());
        
        Method m2 = interfaceSample.getMethods().get(1);
        System.out.println("Name: " + m2.getName() + " expected: speedUp");
        assertEquals("speedUp", m2.getName());
        System.out.println("Return Type: " + m2.getReturnType() + " expected: void");
        assertEquals("void", m2.getReturnType());
        
        Argument arg2 = m2.getArgs().get(0);
        System.out.println("Arguments: " + arg2.getName() + " expected: increment");
        assertEquals("increment", arg2.getName());
        System.out.println("Type: " + arg2.getType() + " expected: int");
        assertEquals("int", arg2.getType());
        System.out.println("\nTest 3 complete");
    }
    
    
}
