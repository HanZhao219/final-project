/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;

import jcd.data.DesignerClassRectangle;
import jcd.data.DesignerData;
import jcd.data.Enums.ProtectionType;
import jcd.data.Enums.StateType;
import jcd.data.Method;
import jcd.data.Variable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Han
 */
public class SaveTest {
    
    ThreadExample instance;
    
    public SaveTest() throws Exception {
        instance = new ThreadExample();
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of saveModifications method, of class ThreadExample.
     */
    @Test(timeout=1000)
    public void testSaveModifications1() throws Exception {
        System.out.println("* UtilsJUnit4Test: test method 1 - testSaveModifications1()");
        ThreadExample instance = new ThreadExample();
        instance.createScenarioOne();
        instance.saveModifications();
        System.out.println("Scenario One is successfully saved.");
    }
    
    @Test(timeout=1000)
    public void testSaveModifications2() throws Exception {
        System.out.println("* UtilsJUnit4Test: test method 2 - testSaveModifications2()");
        ThreadExample instance = new ThreadExample();
        instance.createScenarioTwo();
        instance.saveModifications();
        System.out.println("Scenario Two is successfully saved.");
    }
    
    @Test(timeout=1000)
    public void testSaveModifications3() throws Exception {
        System.out.println("* UtilsJUnit4Test: test method 3 - testSaveModifications3()");
        ThreadExample instance = new ThreadExample();
        instance.createScenarioThree();
        instance.saveModifications();
        System.out.println("Scenario Three is successfully saved.");
    }
}
