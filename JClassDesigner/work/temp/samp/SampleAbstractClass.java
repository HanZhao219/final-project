package samp;

public abstract class SampleAbstractClass{
    public int x;
    public int y;
    public void moveTo(int newX,int newY) {}
    public abstract void draw();
}