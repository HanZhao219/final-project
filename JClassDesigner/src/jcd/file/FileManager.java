package jcd.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import jcd.data.Argument;
import jcd.data.DataManager;
import jcd.data.DesignerClassRectangle;
import jcd.data.DesignerData;
import jcd.data.DesignerInterfaceRectangle;
import jcd.data.Enums.ProtectionType;
import jcd.data.Enums.StateType;
import jcd.data.Method;
import jcd.data.Variable;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class FileManager implements AppFileComponent {
    final static String JSON_TYPE = "Type";
    final static String JSON_XCOR = "Xcor";
    final static String JSON_YCOR = "Ycor";
    final static String JSON_HEIGHT = "Height";
    final static String JSON_WIDTH = "Width";
    final static String JSON_CLASS_NAME = "Name";
    final static String JSON_PACKAGE_NAME = "Package Name";
    final static String JSON_STATE = "State";
    final static String JSON_PROTECTION = "Protection";
    final static String JSON_RETURN_TYPE = "Return Type";
    final static String JSON_METHOD_NAME = "Method Name";
    final static String JSON_METHODS = "Methods";
    final static String JSON_DATA = "Data";
    final static String JSON_X1 = "X1";
    final static String JSON_X2 = "X2";
    final static String JSON_Y1 = "Y1";
    final static String JSON_Y2 = "Y2";
    final static String JSON_VARIABLE_NAME = "Variable Name";
    final static String JSON_VARIABLES = "Variables";
    final static String JSON_ARGUMENTS = "Arguments";
    
    
    public void snapshot(Scene scene) {
        WritableImage image = new WritableImage((int) scene.getWidth(), (int) scene.getHeight());
        scene.snapshot(image);
        
        File file = new File("work/Pose.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            System.out.println("Snapshot saved to test folder.");
        } catch (Exception e) {
        }
    }
    
    
    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        StringWriter sw = new StringWriter();
        
        DataManager dataManager = (DataManager) data;
        // Turn Shapes into 2D array
        
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        
        for (DesignerData currentData : dataManager.getData()) {
            JsonObjectBuilder obj= Json.createObjectBuilder();
            if (currentData instanceof DesignerClassRectangle) {
                DesignerClassRectangle classData = (DesignerClassRectangle) currentData;
                obj = buildClass(classData);
                JsonArrayBuilder variablesArrayBuilder = Json.createArrayBuilder();
                ArrayList<Variable> variables = classData.getVariables();
                
                for (Variable currentVariable : variables) {
                    
                    JsonObject variableNode = buildVariable(currentVariable).build();
                    variablesArrayBuilder.add(variableNode);
                    
                }
                
                obj.add("Variables", variablesArrayBuilder);
                
                JsonArrayBuilder methodsArrayBuilder = Json.createArrayBuilder();
                ArrayList<Method> methods = classData.getMethods();
                
                for (Method currentMethod : methods) {
                    
                    JsonObject methodNode = buildMethod(currentMethod).build();
                    methodsArrayBuilder.add(methodNode);
                    
                }
                
                obj.add("Methods", methodsArrayBuilder);
                
                JsonArrayBuilder packageArrayBuilder = Json.createArrayBuilder();
                for (String packages : currentData.getPackageName()) {
                    
                    JsonObject packageNode = buildPackage(packages).build();
                    packageArrayBuilder.add(packageNode);
                    
                }      
                obj.add("Packages", packageArrayBuilder);
                
                
            } else if (currentData instanceof DesignerInterfaceRectangle) {
                DesignerInterfaceRectangle interfaceData = (DesignerInterfaceRectangle) currentData;
                obj = buildInterface(interfaceData);
                
                JsonArrayBuilder variablesArrayBuilder = Json.createArrayBuilder();
                ArrayList<Variable> variables = interfaceData.getVariables();
                
                for (Variable currentVariable : variables) {
                    
                    JsonObject variableNode = buildVariable(currentVariable).build();
                    variablesArrayBuilder.add(variableNode);
                    
                }
                
                obj.add("Variables", variablesArrayBuilder);
                
                JsonArrayBuilder methodsArrayBuilder = Json.createArrayBuilder();
                ArrayList<Method> methods = interfaceData.getMethods();
                
                for (Method currentMethod : methods) {
                    
                    JsonObject methodNode = buildMethod(currentMethod).build();
                    methodsArrayBuilder.add(methodNode);
                    
                }
                
                obj.add("Methods", methodsArrayBuilder);
                
                JsonArrayBuilder packageArrayBuilder = Json.createArrayBuilder();
                for (String packages : currentData.getPackageName()) {
                    JsonObject packageNode = buildPackage(packages).build();
                    packageArrayBuilder.add(packageNode);
                    
                }      
                obj.add("Packages", packageArrayBuilder);
                
            }
            JsonObject node = obj.build();
            arrayBuilder.add(node);
        }
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add("Data", arrayBuilder)
                .build();
        
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
      
    private JsonObjectBuilder buildClass(DesignerClassRectangle classData) {
        JsonObjectBuilder obj = Json.createObjectBuilder();
        obj.add("Type", "Class");
        obj.add("Xcor", classData.getNormX());
        obj.add("Ycor", classData.getNormY());
        obj.add("Height", classData.getNormH());
        obj.add("Width", classData.getNormW());
        

        obj.add("Name", classData.getClassName());
        obj.add("Package Name", classData.getPackage());
        obj.add("State", classData.getState().name());
        obj.add("Protection", classData.getProtection().name());
        obj.add("Extends", classData.getEC());
        obj.add("Implements", classData.getII());
        
        return obj;
    }
    
    
    private JsonObjectBuilder buildInterface(DesignerInterfaceRectangle interfaceData) {
        JsonObjectBuilder obj = Json.createObjectBuilder();
        obj.add("Type", "Interface");
        obj.add("Xcor", interfaceData.getNormX());
        obj.add("Ycor", interfaceData.getNormY());
        obj.add("Height", interfaceData.getNormH());
        obj.add("Width", interfaceData.getNormW());
        

        obj.add("Name", interfaceData.getClassName());
        obj.add("Package Name", interfaceData.getPackage());
        obj.add("State", interfaceData.getState().name());
        obj.add("Protection", interfaceData.getProtection().name());
        obj.add("Extends", interfaceData.getEC());
        
        return obj;
    }
    
    private JsonObjectBuilder buildMethod (Method currentMethod) {
        JsonObjectBuilder obj = Json.createObjectBuilder();
        
        obj.add("Method Name", currentMethod.getName());
        obj.add("State", currentMethod.getState().name());
        obj.add("Protection", currentMethod.getProtection().name());
        obj.add("Return Type", currentMethod.getReturnType());
        
        JsonArrayBuilder methodsArgArrayBuilder = Json.createArrayBuilder();
        for (Argument n : currentMethod.getArgs()) {
            JsonObjectBuilder argObj = Json.createObjectBuilder();
            argObj.add("Name", n.getName());
            argObj.add("Type", n.getType());
            JsonObject tempObj = argObj.build();
            methodsArgArrayBuilder.add(tempObj);
        }
        
        obj.add("Arguments", methodsArgArrayBuilder);
        
        return obj;
    }
    
    private JsonObjectBuilder buildPackage(String currentPackage) {
        JsonObjectBuilder obj = Json.createObjectBuilder();
        System.out.println(currentPackage);
        obj.add("Package Name", currentPackage);
        return obj;
    }
    
    private JsonObjectBuilder buildVariable (Variable currentVariable) {
        JsonObjectBuilder obj = Json.createObjectBuilder();
        
        obj.add("Variable Name", currentVariable.getName());
        obj.add("Type", currentVariable.getType());
        obj.add("State", currentVariable.getState().name());
        obj.add("Protection", currentVariable.getProtection().name());
        
        return obj;
    }
    
    
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        JsonObject json = loadJSONFile(filePath);
        DataManager dataManager = (DataManager) data;
        
        dataManager.reset();
        // GET THE ARRAY
	JsonArray classArray = json.getJsonArray(JSON_DATA);
        
        for (int i = 0; i<classArray.size(); i++) {
            JsonObject currentPos = classArray.getJsonObject(i);
            if (currentPos.getString(JSON_TYPE).equals("Class")) {
                DesignerData temp = new DesignerClassRectangle();
                temp.setClassName(currentPos.getString(JSON_CLASS_NAME));
                temp.setPackage(currentPos.getString(JSON_PACKAGE_NAME));
                temp.setState(StateType.valueOf(currentPos.getString(JSON_STATE)));
                temp.setProtection(ProtectionType.valueOf(currentPos.getString(JSON_PROTECTION)));
                temp.setNormX(currentPos.getInt(JSON_XCOR));
                temp.setNormY(currentPos.getInt(JSON_YCOR));
                temp.setNormH(currentPos.getInt(JSON_HEIGHT));
                temp.setNormW(currentPos.getInt(JSON_WIDTH));
                temp.setFill(Color.WHITE);
                temp.setStroke(Color.BLACK);
                temp.setEC(currentPos.getString("Extends"));
                ((DesignerClassRectangle)temp).setII(currentPos.getString("Implements"));
                
                
                JsonArray packageArray = currentPos.getJsonArray("Packages");
                
                for (int z=0; z<packageArray.size(); z++) {
                    JsonObject pack = packageArray.getJsonObject(z);
                    temp.getPackageName().add(pack.getString("Package Name"));
                }
                

                JsonArray methodArray = currentPos.getJsonArray(JSON_METHODS);
                for (int m = 0; m<methodArray.size(); m++) {
                    JsonObject currentMethodInfo = methodArray.getJsonObject(m);
                    Method newMethod = temp.addMethod();
                    newMethod.setName(currentMethodInfo.getString(JSON_METHOD_NAME));
                    newMethod.setProtection(ProtectionType.valueOf(currentMethodInfo.getString(JSON_PROTECTION)));
                    newMethod.setState(StateType.valueOf(currentMethodInfo.getString(JSON_STATE)));
                    newMethod.setReturnType(currentMethodInfo.getString(JSON_RETURN_TYPE));
                    
                    JsonArray argumentArray = currentMethodInfo.getJsonArray(JSON_ARGUMENTS);
                    for (int l=0; l<argumentArray.size(); l++) {
                        JsonObject currentArgumentInfo = argumentArray.getJsonObject(l);
                        Argument newArg = new Argument();
                        newArg.setName(currentArgumentInfo.getString(JSON_CLASS_NAME));
                        newArg.setType(currentArgumentInfo.getString(JSON_TYPE));
                        newMethod.addArg(newArg);
                    }
                }
                JsonArray variableArray = currentPos.getJsonArray(JSON_VARIABLES);
                for (int m = 0; m<variableArray.size(); m++) {
                    JsonObject currentVariableInfo = variableArray.getJsonObject(m);
                    Variable newVariable = temp.addVariable();
                    newVariable.setName(currentVariableInfo.getString(JSON_VARIABLE_NAME));
                    newVariable.setType(currentVariableInfo.getString(JSON_TYPE));
                    newVariable.setProtection(ProtectionType.valueOf(currentVariableInfo.getString(JSON_PROTECTION)));
                    newVariable.setState(StateType.valueOf(currentVariableInfo.getString(JSON_STATE)));
                }
                
                dataManager.add(temp);
            } else if (currentPos.getString(JSON_TYPE).equals("Interface")) {
                DesignerData temp = new DesignerInterfaceRectangle();
                temp.setNormX(currentPos.getInt(JSON_XCOR));
                temp.setNormY(currentPos.getInt(JSON_YCOR));
                temp.setNormH(currentPos.getInt(JSON_HEIGHT));
                temp.setNormW(currentPos.getInt(JSON_WIDTH));
                temp.setClassName(currentPos.getString(JSON_CLASS_NAME));
                temp.setPackage(currentPos.getString(JSON_PACKAGE_NAME));
                temp.setState(StateType.valueOf(currentPos.getString(JSON_STATE)));
                temp.setProtection(ProtectionType.valueOf(currentPos.getString(JSON_PROTECTION)));
                temp.setFill(Color.WHITE);
                temp.setStroke(Color.BLACK);
                
                temp.setEC(currentPos.getString("Extends"));
                
                
                JsonArray packageArray = currentPos.getJsonArray("Packages");
                for (int z=0; z<packageArray.size(); z++) {
                    temp.getPackageName().add(packageArray.getString(z));
                }
                
                
                JsonArray methodArray = currentPos.getJsonArray(JSON_METHODS);
                for (int m = 0; m<methodArray.size(); m++) {
                    JsonObject currentMethodInfo = methodArray.getJsonObject(m);
                    Method newMethod = temp.addMethod();
                    newMethod.setName(currentMethodInfo.getString(JSON_METHOD_NAME));
                    newMethod.setProtection(ProtectionType.valueOf(currentMethodInfo.getString(JSON_PROTECTION)));
                    newMethod.setState(StateType.valueOf(currentMethodInfo.getString(JSON_STATE)));
                    newMethod.setReturnType(currentMethodInfo.getString(JSON_RETURN_TYPE));
                    
                    JsonArray argumentArray = currentMethodInfo.getJsonArray(JSON_ARGUMENTS);
                    for (int l=0; l<argumentArray.size(); l++) {
                        JsonObject currentArgumentInfo = argumentArray.getJsonObject(l);
                        Argument newArg = new Argument();
                        newArg.setName(currentArgumentInfo.getString(JSON_CLASS_NAME));
                        newArg.setType(currentArgumentInfo.getString(JSON_TYPE));
                        newMethod.addArg(newArg);
                    }
                }
                JsonArray variableArray = currentPos.getJsonArray(JSON_VARIABLES);
                for (int m = 0; m<variableArray.size(); m++) {
                    JsonObject currentVariableInfo = variableArray.getJsonObject(m);
                    Variable newVariable = temp.addVariable();
                    newVariable.setName(currentVariableInfo.getString(JSON_VARIABLE_NAME));
                    newVariable.setType(currentVariableInfo.getString(JSON_TYPE));
                    newVariable.setProtection(ProtectionType.valueOf(currentVariableInfo.getString(JSON_PROTECTION)));
                    newVariable.setState(StateType.valueOf(currentVariableInfo.getString(JSON_STATE)));
                }
                
                dataManager.add(temp);
                
            }
            
        }
        dataManager.re();
        dataManager.re();
    }
    


    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {

    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }

    @Override
    public void saveJavaData(AppDataComponent data, String filePath) throws IOException {
        File file = new File(filePath);
        // If it doesn't exist, make the folder
        if (!file.exists()) file.mkdir();
        
        DataManager dataManager = (DataManager) data;
        for (DesignerData n : dataManager.getData()) {
            if (n instanceof DesignerClassRectangle) {
                
                String packageName = n.getPackage();
                String[] packages = packageName.split("\\.");
                String tempPath = filePath; 
                for (int i=0; i<packages.length; i++) {
                    tempPath = tempPath + "\\" + packages[i] + "\\";
                    System.out.println(tempPath);
                    file = new File(tempPath);
                    if (!file.exists()) file.mkdir();
                }
                
                tempPath = tempPath + "\\" + n.getClassName() + ".java";
                
                file = new File(tempPath);
                file.createNewFile();
                
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write("package " + packageName + ";");
                bw.write("\n\n");
                for (String nx: n.getPackageName()) {
                    bw.write("import " + nx + ";\n");
                }
                if (n.getProtection() != ProtectionType.NO_MODIFIER) bw.write(n.getProtection().toString() + " ");
                if (n.getState() != StateType.NONE) bw.write(n.getState().toString() + " ");
                bw.write("class " + n.getClassName());
                if (!n.getEC().equals("")) bw.write(" extends " + n.getEC());
                if (!((DesignerClassRectangle)n).getII().equals("")) bw.write(" implements " + ((DesignerClassRectangle)n).getII());
                bw.write("{\n");
                
                for (Variable variable : n.getVariables()) {
                    bw.write("    ");
                    if (variable.getProtection() != ProtectionType.NO_MODIFIER) bw.write(variable.getProtection().toString() + " ");
                    if (variable.getState() != StateType.NONE) bw.write(variable.getState().toString() + " ");
                    bw.write(variable.getType() + " " + variable.getName() + ";\n");
                }
                
                for (Method method : n.getMethods()) {
                    bw.write("    ");
                    if (method.getProtection() != ProtectionType.NO_MODIFIER) bw.write(method.getProtection().toString() + " ");
                    if (method.getState() != StateType.NONE) bw.write(method.getState().toString() + " ");
                    bw.write(method.getReturnType() + " " + method.getName() + "(");
                    for (int x=0; x<method.getArgs().size(); x++) {
                        Argument arg = method.getArgs().get(x);
                        bw.write(arg.getType() + " " + arg.getName());
                        if (x < method.getArgs().size() - 1) bw.write(",");
                    }
                    bw.write(")");
                    if (method.getState() != StateType.ABSTRACT) {
                        bw.write(" {}");
                    } else {
                        bw.write(";");
                    }
                    bw.write("\n");
                }
                
                bw.write("}");
                bw.close();
            } else if (n instanceof DesignerInterfaceRectangle) {
                
                String packageName = n.getPackage();
                String[] packages = packageName.split("\\.");
                String tempPath = filePath; 
                for (int i=0; i<packages.length; i++) {
                    tempPath = tempPath + "\\" + packages[i] + "\\";
                    System.out.println(tempPath);
                    file = new File(tempPath);
                    if (!file.exists()) file.mkdir();
                }
                
                tempPath = tempPath + "\\" + n.getClassName() + ".java";
                
                file = new File(tempPath);
                file.createNewFile();
                
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write("package " + packageName + ";");
                bw.write("\n\n");
                for (String nx: n.getPackageName()) {
                    bw.write("import " + nx + ";\n");
                }
                if (n.getProtection() != ProtectionType.NO_MODIFIER) bw.write(n.getProtection().toString() + " ");
                if (n.getState() != StateType.NONE) bw.write(n.getState().toString() + " ");
                bw.write("interface " + n.getClassName());
                if (!n.getEC().equals("")) bw.write(" extends " + n.getEC());
                bw.write("{\n");
                
                for (Variable variable : n.getVariables()) {
                    bw.write("    ");
                    if (variable.getProtection() != ProtectionType.NO_MODIFIER) bw.write(variable.getProtection().toString() + " ");
                    if (variable.getState() != StateType.NONE) bw.write(variable.getState().toString() + " ");
                    bw.write(variable.getType() + " " + variable.getName() + ";\n");
                }
                
                for (Method method : n.getMethods()) {
                    bw.write("    ");
                    if (method.getProtection() != ProtectionType.NO_MODIFIER) bw.write(method.getProtection().toString() + " ");
                    if (method.getState() != StateType.NONE) bw.write(method.getState().toString() + " ");
                    bw.write(method.getReturnType() + " " + method.getName() + "(");
                    for (int x=0; x<method.getArgs().size(); x++) {
                        Argument arg = method.getArgs().get(x);
                        bw.write(arg.getType() + " " + arg.getName());
                        if (x < method.getArgs().size() - 1) bw.write(",");
                    }
                    bw.write(")");
                    if (method.getState() != StateType.ABSTRACT) {
                        bw.write(" {}");
                    } else {
                        bw.write(";");
                    }
                    bw.write("\n");
                }
                
                bw.write("}");
                bw.close();
            }
            
            
            
            
        }
        
        
        
    }
    
     
}
