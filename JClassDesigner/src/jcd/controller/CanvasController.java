/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.controller;

import javafx.scene.paint.Color;
import jcd.data.DataManager;
import jcd.data.DesignerClassRectangle;
import jcd.data.DesignerData;
import jcd.data.DesignerInterfaceRectangle;
import jcd.file.FileManager;
import jcd.gui.Workspace;
import saf.AppTemplate;

/**
 *
 * @author Han
 */
public class CanvasController {
    AppTemplate app;
    
    public CanvasController(AppTemplate initApp) {
	app = initApp;
    }
    
    public void processGrid(boolean enabled) {
        DataManager dataManager = (DataManager)app.getDataComponent();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        dataManager.enableGrid(enabled);
        workspace.reloadWorkspace();
    }
    
    public void processSnap(boolean enabled) {
        DataManager dataManager = (DataManager)app.getDataComponent();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        dataManager.enableSnap(enabled);
        workspace.reloadWorkspace();
    }
    
    public void processZoomInRequest() {
        DataManager dataManager = (DataManager)app.getDataComponent();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        dataManager.setZoom(dataManager.getZoom()+1);
        workspace.reloadWorkspace();
    }
    
    public void processZoomOutRequest() {
        DataManager dataManager = (DataManager)app.getDataComponent();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        dataManager.setZoom(dataManager.getZoom()-1);
        workspace.reloadWorkspace();
    }

    public void processCanvasMousePress(double x, double y) {
        DataManager dataManager = (DataManager)app.getDataComponent();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        double zoomLevel = Math.pow(1.2, dataManager.getZoom()*-1);
        if (dataManager.isInState(JClassDesignerState.CREATING_CLASS)) {
            DesignerClassRectangle temp = new DesignerClassRectangle(200, 300);
            temp.setFill(Color.WHITE);
            temp.setStroke(Color.BLACK);
            if (dataManager.getSnapEnablede()) {
                int multx = (int) (x/50);
                int multy = (int) (y/50);
                temp.setX((multx*50)*zoomLevel);
                temp.setY((multy*50)*zoomLevel);
            } else {
                temp.setX(x*zoomLevel);
                temp.setY(y*zoomLevel);
            }
            dataManager.add(temp);
            dataManager.setSelect(temp);
            workspace.clearFields();
            if (dataManager.getSnapEnablede()) {
                temp.update();
                int multx = (int) (x/50);
                int multy = (int) (y/50);
                temp.setNormX((multx*50));
                temp.setNormY((multy*50));
            } else {
                temp.update();
            }
            app.getGUI().markAsEdit();
        } else if (dataManager.isInState(JClassDesignerState.CREATING_INTERFACE)) {
            DesignerInterfaceRectangle temp = new DesignerInterfaceRectangle(200, 300);
            temp.setFill(Color.WHITE);
            temp.setStroke(Color.BLACK);
            if (dataManager.getSnapEnablede()) {
                int multx = (int) (x/50);
                int multy = (int) (y/50);
                temp.setX((multx*50)*zoomLevel);
                temp.setY((multy*50)*zoomLevel);
            } else {
                temp.setX(x*zoomLevel);
                temp.setY(y*zoomLevel);
            };
            dataManager.add(temp);
            dataManager.setSelect(temp);
            if (dataManager.getSnapEnablede()) {
                temp.update();
                int multx = (int) (x/50);
                int multy = (int) (y/50);
                temp.setNormX((multx*50));
                temp.setNormY((multy*50));
            } else {
                temp.update();
            }
            workspace.clearFields();
            app.getGUI().markAsEdit();
        } else if (dataManager.isInState(JClassDesignerState.SELECTING_OBJECT)) {
            dataManager.select(x, y);
            dataManager.setNewSelected(true);
            app.getGUI().markAsEdit();
        }
	workspace.reloadWorkspace();
    }
    
    public void processCanvasMouseDrag(double x, double y) {
        DataManager dataManager = (DataManager)app.getDataComponent();
        double zoomLevel = Math.pow(1.2, dataManager.getZoom()*-1);
        if (dataManager.isInState(JClassDesignerState.SELECTING_OBJECT) &&
            dataManager.getSelected().contains(x, y)) {
            dataManager.changeState(JClassDesignerState.MOVING_OBJECT);
        } else if (dataManager.isInState(JClassDesignerState.MOVING_OBJECT)) {
            if (x >= 0) dataManager.getSelected().setNormX(x*zoomLevel);
            if (y >= 0) dataManager.getSelected().setNormY(y*zoomLevel);
        } else if (dataManager.isInState(JClassDesignerState.RESIZING_OBJECT)) {
            double wid = (x - dataManager.getSelected().getX())*zoomLevel;
            double hid = (y - dataManager.getSelected().getY()*zoomLevel);
            if (wid > 100) dataManager.getSelected().setNormW((x - dataManager.getSelected().getX())*zoomLevel);
            if (hid > 100) dataManager.getSelected().setNormH((y - dataManager.getSelected().getY()*zoomLevel));
            if (wid < 100) dataManager.getSelected().setNormW(100 * zoomLevel);
            if (hid < 100) dataManager.getSelected().setNormH(100 * zoomLevel);
        }
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
    }
    
    public void processCanvasMouseRelease(double x, double y) {
        DataManager dataManager = (DataManager)app.getDataComponent();
        if (dataManager.isInState(JClassDesignerState.MOVING_OBJECT)) {
            dataManager.changeState(JClassDesignerState.SELECTING_OBJECT);
        }
    }
    
    public void processClassNameChange(String text) {
        DataManager dataManager = (DataManager)app.getDataComponent();
        
        if (dataManager.isSelectedNull()) throw new NullPointerException();
        dataManager.changeSelectedClassName(text);
        
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
    }

    
    public void remove() {
        DataManager dataManager = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        if (!dataManager.isSelectedNull()) {
            dataManager.removeSelected();
        }
        workspace.reloadWorkspace();
    }
    
}
