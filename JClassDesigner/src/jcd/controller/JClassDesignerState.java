/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.controller;

/**
 *
 * @author Han
 */
    
public enum JClassDesignerState {
    CREATING_CLASS,
    CREATING_INTERFACE,
    SELECTING_OBJECT,
    MOVING_OBJECT,
    RESIZING_OBJECT
}
