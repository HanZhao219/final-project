/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import jcd.data.Enums.ProtectionType;
import jcd.data.Enums.StateType;

/**
 *
 * @author Han
 */
public class Method {
    private ProtectionType protection;
    private String name;
    private StateType state;
    private String returnType;
    private ArrayList<Argument> args;
    
    
    public Method() {
        protection = ProtectionType.PUBLIC;
        name = "";
        state = StateType.NONE;
        returnType = null;
        args = new ArrayList();
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setProtection(ProtectionType protection) {
        this.protection = protection;
    }
    
    public ProtectionType getProtection() {
        return protection;
    }
    
    public void setState(StateType state) {
        this.state = state;
    }
    
    public StateType getState() {
        return state;
    }
    
    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }
    
    public String getReturnType() {
        return returnType;
    }
    
    public ArrayList<Argument> getArgs() {
        return args;
    }
    
    public void addArg(Argument n) {
        args.add(n);
    }
    
    
}
