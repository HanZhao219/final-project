/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.geometry.Point2D;
import javafx.scene.shape.Rectangle;
import jcd.data.Enums.ProtectionType;
import jcd.data.Enums.StateType;

/**
 *
 * @author Han
 */
public class DesignerData extends Rectangle{
    private String className;
    private String packageN;
    private ArrayList<String> packageName;
    private ArrayList<Method> methods;
    private ArrayList<Variable> variables;
    private StateType state;
    private ProtectionType protection;
    private double normX, normY, normH, normW;
    private ArrayList<Point2D> connectorPoints;
    private String extendedClass;
    
    public DesignerData() {
        className = "";
        packageName = new ArrayList();
        defaultInit();
    }
    
    public DesignerData(double width, double height) {
        super(width, height);
        className = "";
        packageName = new ArrayList();
        defaultInit();
    }
    
    public void defaultInit() {
        methods = new ArrayList();
        variables = new ArrayList();
        state = StateType.NONE;
        protection = ProtectionType.NO_MODIFIER;
        connectorPoints = new ArrayList();
        extendedClass = "";
    }
    
    public String getClassName() {
        return className;
    }
    
    public ArrayList<String> getPackageName() {
        return packageName;
    }
    
    public void setClassName(String newClassName) {
        className = newClassName;
    }
    
    public void addPackageName(String newPackageName) {
        packageName.add(newPackageName);
    }
    
    public Method addMethod() {
        Method currentMethod = new Method();
        methods.add(currentMethod);
        return currentMethod;
    }
    
    public Method getMethod(String methodName) {
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                return method;
            }
        }
        return null;
    }
    
    public ArrayList<Method> getMethods() {
        return methods;
    }
    
    public Variable addVariable() {
        Variable newVariable = new Variable();
        variables.add(newVariable);
        return newVariable;
    }
    
    public ArrayList<Variable> getVariables() {
        return variables;
    }
    
    public void setProtection(ProtectionType protection) {
        this.protection = protection;
    }
    
    public ProtectionType getProtection() {
        return protection;
    }
    
    public void setState(StateType state) {
        this.state = state;
    }
    
    public StateType getState() {
        return state;
    }

    public void setExtendedClass(DesignerClassRectangle designerClassRectangle) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void update() {
        normX = this.getX();
        normY = this.getY();
        normW = this.getWidth();
        normH = this.getHeight();
    }
    
    public double getNormX() {
        return normX;
    }
    
    public double getNormY() {
        return normY;
    }
    
    public double getNormW() {
        return normW;
    }
    
    public double getNormH() {
        return normH;
    }
    
    public void setNormX(double newX) {
        normX = newX;
    }
    
    public void setNormY(double newY) {
        normY = newY;
    }
    
    public void setNormH(double newH) {
        normH = newH;
    }
    
    public void setNormW(double newW) {
        normW = newW;
    }
    
    public String getPackage() {
        return packageN;
    }
    
    public void setPackage(String n) {
        packageN = n;
    }
    public ArrayList<Point2D> getPoints() {
        return connectorPoints;
    }
    public String getEC() {
        return extendedClass;
    }
    public void setEC(String newEC) {
        extendedClass = newEC;
    }
}
