/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;



/**
 *
 * @author Han
 */
public class DesignerClassRectangle extends DesignerData{
    private String implementedInterfaces;
    
    public DesignerClassRectangle() {
        super();
        implementedInterfaces = "";
        
    }
    
    public DesignerClassRectangle(double width, double height) {
        super(width, height);
        implementedInterfaces = "";
    }
    /*
    public void setExtendedClass(DesignerClassRectangle extendedClass) {
        this.extendedClass = extendedClass;
    }
    
    public DesignerClassRectangle getExtendedClass() {
        return extendedClass;
    }
    
    public void addImplementedInterface(DesignerInterfaceRectangle implementedInterface) {
        implementedInterfaces.add(implementedInterface);
    }
    
    public ArrayList<DesignerInterfaceRectangle> getImplementedInterfaces() {
        return implementedInterfaces;
    }*/
    
    public String getII() {
        return implementedInterfaces;
    }
    public void setII(String newI) {
        implementedInterfaces = newI;
    }
}
