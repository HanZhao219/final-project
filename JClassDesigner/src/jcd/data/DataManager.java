package jcd.data;

import java.util.ArrayList;
import jcd.controller.JClassDesignerState;
import jcd.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    double zoom;
    boolean zoomIn;
    JClassDesignerState currentState;
    GridCanvas canvasData;
    DesignerData selected;
    boolean newSelected;
    boolean gridEnabled;
    boolean snapEnabled;
    
    /**
     * This constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        canvasData = new GridCanvas();
        newSelected = false;
        gridEnabled = false;
        snapEnabled =false;
        zoom = 0;
        //
    }
    
    public void changeState(JClassDesignerState newState) {
        currentState = newState;
    }
    
    public GridCanvas getGrid() {
        return canvasData;
    }
    
    public boolean isInState(JClassDesignerState state) {
        return currentState == state;
    }

    public void add(DesignerData newData) {
        canvasData.add(newData);
    }
    
    public void setNewSelected(boolean yesno) {
        newSelected = yesno;
    }
    
    public boolean isNewSelected() {
        return newSelected;
    }

    public void select(double xCor, double yCor) {
        selected = canvasData.find(xCor, yCor);
    }
    
    public void setSelect(DesignerData newSelected) {
        selected = newSelected;
    }
    
    public void removeSelected() {
        canvasData.remove(selected);
    }
    
    public DesignerData getSelected() {
        return selected;
    }
    
    public ArrayList<DesignerData> getData() {
        return canvasData.getData();
    }
    
    public boolean isSelectedNull() {
        return selected == null;
    }
    
    public boolean changeSelectedClassName(String newName) {
        if (selected != null) {
            selected.setClassName(newName);
            return true;
        }
        return false;
    }
    
    
    public double getZoom() {
        return zoom;
    }
    
    public void setZoom(double newZoom) {
        zoom = newZoom;
    }
    
    public void enableGrid(boolean enable) {
        gridEnabled = enable;
    }
    
    public boolean getGridEnabled() {
        return gridEnabled;
    }
    
    public void enableSnap(boolean enable) {
        snapEnabled = enable;
    }
    
    public boolean getSnapEnablede() {
        return snapEnabled;
    }
    
    public void re() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.reloadWorkspace();
    }
    
    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        canvasData = new GridCanvas();
    }
}
