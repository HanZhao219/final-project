/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import jcd.data.Enums.ProtectionType;
import jcd.data.Enums.StateType;

/**
 *
 * @author Han
 */
public class Variable extends Argument{
    private ProtectionType protection;
    private StateType state;
    
    
    public Variable() {
        super();
        protection = ProtectionType.PUBLIC;
        state = StateType.NONE;
    }
    
    
    public void setProtection(ProtectionType protection) {
        this.protection = protection;
    }
    
    public ProtectionType getProtection() {
        return protection;
    }
    
    public void setState(StateType state) {
        this.state = state;
    }
    
    public StateType getState() {
        return state;
    }

    
    
    
    
}

