/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;

/**
 *
 * @author Han
 */
public class GridCanvas {
    
    private ArrayList<DesignerData> data;
    // implement scale for scaling
    private double scale;
    
    public GridCanvas() {
        data = new ArrayList<DesignerData>();
    }
    
    public DesignerData find(double xCor, double yCor) {
        for (DesignerData n:data) {
            if (n.contains(xCor, yCor)) return n;
        }
        return null;
    }
    
    public void add(DesignerData newData) {
        data.add(newData);
    }
    
    public void remove(DesignerData deleteData) {
        data.remove(deleteData);
    }
    
    public ArrayList<DesignerData> getData() {
        return data;
    }
}
