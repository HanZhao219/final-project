/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data.Enums;

/**
 *
 * @author Han
 */
public enum ProtectionType {
    PUBLIC("public"),
    PROTECTED("protected"),
    PRIVATE("private"),
    NO_MODIFIER("");
    
    private String name;
    
    private ProtectionType(String string) {
        this.name = string;
    }

    public String toString() {
       return this.name;
    }
}
