/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data.Enums;

/**
 *
 * @author Han
 */
public enum StateType {
    STATIC ("static"),
    ABSTRACT ("abstract"),
    NONE ("");
    
    private String name;
    
    private StateType(String string) {
        this.name = string;
    }

    public String toString() {
       return this.name;
    }
}
