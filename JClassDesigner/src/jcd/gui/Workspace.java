package jcd.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import jcd.JClassDesigner;
import static jcd.PropertyType.ADD_CLASS_TOOL_ICON;
import static jcd.PropertyType.ADD_CLASS_TOOL_TOOLTIP;
import static jcd.PropertyType.ADD_INTERFACE_TOOL_ICON;
import static jcd.PropertyType.ADD_INTERFACE_TOOL_TOOLTIP;
import static jcd.PropertyType.REDO_TOOL_ICON;
import static jcd.PropertyType.REDO_TOOL_TOOLTIP;
import static jcd.PropertyType.REMOVE_TOOL_ICON;
import static jcd.PropertyType.REMOVE_TOOL_TOOLTIP;
import static jcd.PropertyType.RESIZE_TOOL_ICON;
import static jcd.PropertyType.RESIZE_TOOL_TOOLTIP;
import static jcd.PropertyType.SELECT_TOOL_ICON;
import static jcd.PropertyType.SELECT_TOOL_TOOLTIP;
import static jcd.PropertyType.TOOLS_ICON;
import static jcd.PropertyType.TOOLS_TOOLTIP;
import static jcd.PropertyType.UNDO_TOOL_ICON;
import static jcd.PropertyType.UNDO_TOOL_TOOLTIP;
import static jcd.PropertyType.VIEW_ICON;
import static jcd.PropertyType.VIEW_TOOLTIP;
import static jcd.PropertyType.ZOOM_IN_TOOL_ICON;
import static jcd.PropertyType.ZOOM_IN_TOOL_TOOLTIP;
import static jcd.PropertyType.ZOOM_OUT_TOOL_ICON;
import static jcd.PropertyType.ZOOM_OUT_TOOL_TOOLTIP;
import jcd.controller.CanvasController;
import jcd.controller.JClassDesignerController;
import static jcd.controller.JClassDesignerState.CREATING_CLASS;
import static jcd.controller.JClassDesignerState.CREATING_INTERFACE;
import static jcd.controller.JClassDesignerState.RESIZING_OBJECT;
import static jcd.controller.JClassDesignerState.SELECTING_OBJECT;
import jcd.data.Argument;
import jcd.data.DataManager;
import jcd.data.DesignerClassRectangle;
import jcd.data.DesignerData;
import jcd.data.DesignerInterfaceRectangle;
import jcd.data.Enums.ProtectionType;
import jcd.data.Enums.StateType;
import jcd.data.Method;
import jcd.data.Variable;
import jcd.file.FileManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    static final String CLASS_MAIN_TOOLBARS = "main_toolbar";
    static final String CLASS_SUB_TOOLBARS = "sub_toolbar";
    static final String CLASS_EDIT_MENU = "edit_menu";
    static final String CLASS_FONT = "text_font";
    static final String CLASS_BUTTON = "button_format";
    static final String CLASS_CANVAS = "canvas";
    static final String CLASS_CLASS = "class_class";

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    FlowPane toolContainerPane;

    ToggleGroup toolGroup;
    ToggleButton tools;
    ToggleButton view;

    StackPane toolbarContainerPane;
    HBox toolbarPane;
    HBox viewPane;

    Button selectButton;
    Button resizeButton;
    Button addClassButton;
    Button addInterfaceButton;
    Button removeButton;
    Button undoButton;
    Button redoButton;

    VBox editClassElements;
    Label className;
    Label packageName;
    Label parentName;
    Label variables;
    Label methods;
    TextField classNameField;
    TextField packageField;
    
    ComboBox parentBox;
    TextField parentNameField;
    Button snapshot;
    
    HBox mainPackageMenu;
    VBox packageControls;
    ScrollPane packageListContainer;
    VBox packages;
    ToggleGroup packageGroup;
    Button addPackage;
    Button removePackage;
    
    ScrollPane variablesScrollPane;
    GridPane variablesPane;
    ScrollPane methodsScrollPane;
    GridPane methodsPane;


    ScrollPane canvasPaneContainer;
    Pane canvasPane;


    Button zoomInButton;
    Button zoomOutButton;
    CheckBox gridBox;
    CheckBox snapBox;

    DataManager dataManager;
    FileManager fileManager;
    JClassDesignerController pageController;
    CanvasController canvasController;


    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        // KEEP THIS FOR LATER
        app = initApp;

        // KEEP THE GUI FOR LATER
        gui = app.getGUI();

        dataManager = (DataManager) app.getDataComponent();
        fileManager = (FileManager)app.getFileComponent();
        pageController = new JClassDesignerController((JClassDesigner) app);
        canvasController = new CanvasController((JClassDesigner) app);


        initGUI();
        assignButtons();
    }


    public void initGUI() {
        initToolbar();
        initEditWindow();




        // Put everything on GUI
        toolbarContainerPane = new StackPane();
        toolbarContainerPane.getChildren().addAll(toolbarPane, viewPane);
        workspace = new StackPane();

        


        canvasPane = new Pane();
        canvasPane.setMinHeight(2000);
        canvasPane.setMinWidth(2000);

        canvasPaneContainer = new ScrollPane();
        canvasPaneContainer.setContent(canvasPane);

        //canvasPaneContainer.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        //canvasPaneContainer.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        gui.getAppPane().setVgrow(workspace, Priority.ALWAYS);

        ((StackPane)workspace).setAlignment(editClassElements, Pos.TOP_RIGHT);
        editClassElements.setMaxWidth(450);
        ((StackPane)workspace).getChildren().addAll(canvasPaneContainer, editClassElements);
        editClassElements.setVisible(false);
        gui.getAppPane().getChildren().addAll(toolContainerPane, toolbarContainerPane);
    }

    public void initToolbar() {
        toolContainerPane = new FlowPane();

        toolGroup = new ToggleGroup();

        // Create main menu buttons for selecting either the tools or view menu
        tools = gui.initChildToggleButton("Tools", toolContainerPane, TOOLS_ICON.toString(), TOOLS_TOOLTIP.toString(), false);
        view = gui.initChildToggleButton("View", toolContainerPane, VIEW_ICON.toString(), VIEW_TOOLTIP.toString(), false);

        tools.setToggleGroup(toolGroup);
        view.setToggleGroup(toolGroup);
        tools.setSelected(true);

        // Create submenu and submenu buttons
        toolbarPane = new HBox();
        viewPane = new HBox();
        viewPane.setVisible(false);

        selectButton = gui.initChildButton("Select", toolbarPane, SELECT_TOOL_ICON.toString(), SELECT_TOOL_TOOLTIP.toString(), false);
        resizeButton = gui.initChildButton("Resize", toolbarPane, RESIZE_TOOL_ICON.toString(), RESIZE_TOOL_TOOLTIP.toString(), true);
        addClassButton = gui.initChildButton("Add Class", toolbarPane, ADD_CLASS_TOOL_ICON.toString(), ADD_CLASS_TOOL_TOOLTIP.toString(), false);
        addInterfaceButton = gui.initChildButton("Add Interface", toolbarPane, ADD_INTERFACE_TOOL_ICON.toString(), ADD_INTERFACE_TOOL_TOOLTIP.toString(), false);
        removeButton = gui.initChildButton("Remove", toolbarPane, REMOVE_TOOL_ICON.toString(), REMOVE_TOOL_TOOLTIP.toString(), true);
        undoButton = gui.initChildButton("Undo", toolbarPane, UNDO_TOOL_ICON.toString(), UNDO_TOOL_TOOLTIP.toString(), true);
        redoButton = gui.initChildButton("Redo", toolbarPane, REDO_TOOL_ICON.toString(), REDO_TOOL_TOOLTIP.toString(), true);

        zoomOutButton = gui.initChildButton("Zoom Out", viewPane, ZOOM_OUT_TOOL_ICON.toString(), ZOOM_OUT_TOOL_TOOLTIP.toString(), false);
        zoomInButton = gui.initChildButton("Zoom In", viewPane, ZOOM_IN_TOOL_ICON.toString(), ZOOM_IN_TOOL_TOOLTIP.toString(), false);
        snapshot = new Button("Snapshot");
        snapshot.setOnAction(e->{
            fileManager.snapshot(gui.getPrimaryScene());
        });
        
        gridBox = new CheckBox("Grid");
        snapBox = new CheckBox("Snap");
        viewPane.getChildren().addAll(gridBox, snapBox, snapshot);
    }

    public void initEditWindow() {
        editClassElements = new VBox();
        className = new Label("Class Name: ");
        packageName = new Label("Packages: ");
        parentName = new Label("Parent: ");
        variables = new Label ("Variable: ");
        methods = new Label("Methods: ");

        classNameField = new TextField();
        
        
        mainPackageMenu = new HBox();
        mainPackageMenu.setPadding(new Insets(10));
        
        
        packageControls = new VBox();
        addPackage = new Button("Add Package");
        addPackage.setMinWidth(110);
        removePackage = new Button("Remove Package");
        removePackage.setMinWidth(110);
        packageControls.getChildren().addAll(addPackage, removePackage);
        addPackage.setOnAction(e-> {
            popOne();
            reloadWorkspace();
        });
        removePackage.setOnAction(e->{
            packages.getChildren().remove((ToggleButton) packageGroup.getSelectedToggle());
            dataManager.getSelected().getPackageName().remove(packageGroup.getSelectedToggle().getUserData().toString());
            packageGroup.getToggles().remove(packageGroup.getSelectedToggle());
            reloadWorkspace();
        });
        
        packages = new VBox();
        packageGroup = new ToggleGroup();
        packages.setMinSize(200, 100);
        packageListContainer = new ScrollPane(packages);
        packageListContainer.setMinSize(300, 50);
        packageListContainer.setMaxSize(300, 50);
        mainPackageMenu.getChildren().addAll(packageControls, packageListContainer);
        
        
        packageField = new TextField();
        
        
        parentBox = new ComboBox();
        parentNameField = new TextField();
        variablesScrollPane = new ScrollPane();

        variablesPane = new GridPane();
        variablesScrollPane.setContent(variablesPane);
        variablesPane.setMinSize(0, 300);
        variablesScrollPane.setMinHeight(200);
        variablesPane.setPadding(new Insets(10,40,10,15));
        variablesScrollPane.setFitToHeight(true);

        reGridVariables();

        methodsPane = new GridPane();

        methodsPane.setMinSize(0, 300);
        methodsPane.setPadding(new Insets(10,40,10,15));

        methodsScrollPane = new ScrollPane(methodsPane);
        methodsScrollPane.setMinHeight(200);
        methodsScrollPane.setFitToHeight(true);

        reGridMethods();

        editClassElements.getChildren().addAll(className, classNameField, 
                packageName, packageField, mainPackageMenu, parentName, parentBox, parentNameField,
                variables, variablesScrollPane, methods, methodsScrollPane);

    }

    public void reGridVariables() {
        Label nameText = new Label("Name");
        nameText.setPadding(new Insets(10));
        Label staticText = new Label("Static");
        staticText.setPadding(new Insets(10));
        Label typeText = new Label("Type");
        typeText.setPadding(new Insets(10));
        Label accessText = new Label("Access");
        accessText.setPadding(new Insets(10));
        variablesPane.add(nameText, 0, 0);
        variablesPane.add(typeText, 1, 0);
        variablesPane.add(staticText, 2, 0);
        variablesPane.add(accessText, 3, 0);
    }

    public void reGridMethods() {
        Label nameText = new Label("Name");
        nameText.setPadding(new Insets(10));
        Label staticText = new Label("Static/Abstract");
        staticText.setPadding(new Insets(10));
        Label typeText = new Label("Type");
        typeText.setPadding(new Insets(10));
        Label accessText = new Label("Access");
        accessText.setPadding(new Insets(10));
        methodsPane.add(nameText, 0, 0);
        methodsPane.add(typeText, 1, 0);
        methodsPane.add(staticText, 2, 0);
        methodsPane.add(accessText, 3, 0);
        if (!dataManager.isSelectedNull()) {
            int currentMax = 0;
            for (Method m: dataManager.getSelected().getMethods()) {
                if (m.getArgs().size()>currentMax) currentMax = m.getArgs().size();
            }
            int counter=4;
            for (int zx=0; zx<= currentMax; zx++) {
                Text arg = new Text("arg" + (zx + 1));
                methodsPane.add(arg, counter, 0);
                counter++;
            }
        }
    }
    
    public void popOne() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Creating Package");
        dialog.setHeaderText("Choosing the package location");
        dialog.setContentText("Please enter the package location");
        
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(e->{
            RadioButton rb1 = new RadioButton(result.get());
            rb1.setUserData(result.get());
            rb1.setToggleGroup(packageGroup);
            dataManager.getSelected().addPackageName(result.get());
            packages.getChildren().add(rb1);
        });
    }
    
    public void setUpPackages() {
        packageGroup = new ToggleGroup();
        packages.getChildren().clear();
        
        for (String n: dataManager.getSelected().getPackageName()) {
            RadioButton x = new RadioButton(n);
            x.setUserData(n);
            x.setToggleGroup(packageGroup);
            packages.getChildren().add(x); 
        }
    }
    
    public void assignButtons() {
        tools.setOnAction(e->{
            // Set all panes to invisible
            for (Node i: toolbarContainerPane.getChildren()) {
                if (i instanceof Pane) {
                i.setVisible(false);
            }
            }

            // Make itself visible
            toolbarPane.setVisible(true);
        });
        view.setOnAction(e-> {
            // Set all panes to invisible
            for (Node i: toolbarContainerPane.getChildren()) {
                if (i instanceof Pane) {
                i.setVisible(false);
            }
            }

            // Make itself visible
            viewPane.setVisible(true);
        });

        addClassButton.setOnAction(e-> {
            dataManager.changeState(CREATING_CLASS);
        });
        addInterfaceButton.setOnAction(e-> {
            dataManager.changeState(CREATING_INTERFACE);
        });
        selectButton.setOnAction(e-> {
            dataManager.changeState(SELECTING_OBJECT);
        });
        resizeButton.setOnAction(e-> {
            dataManager.changeState(RESIZING_OBJECT);
        });
        classNameField.setOnKeyReleased(e-> {
            try {
                canvasController.processClassNameChange(classNameField.getText());
            } catch (NullPointerException f) {
                classNameField.clear();
            }
        });
        canvasPane.setOnMousePressed((MouseEvent e)-> {
            canvasController.processCanvasMousePress(e.getX(), e.getY());
            editClassElements.setVisible(true);
        });
        canvasPane.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                try {
                    canvasController.processCanvasMouseDrag(e.getX(), e.getY());
                } catch (Exception x) {
                    reloadWorkspace();
                }
            }
        });
        packageField.setOnKeyReleased(e-> {
            dataManager.getSelected().setPackage(packageField.getText());
        });
        canvasPane.setOnMouseReleased((MouseEvent e)-> {
            canvasController.processCanvasMouseRelease(e.getX(), e.getY());
            if (!dataManager.isSelectedNull()) {
                resizeButton.setDisable(false);
                removeButton.setDisable(false);
            } else {
                resizeButton.setDisable(true);
                removeButton.setDisable(true);
            }
        });
        zoomInButton.setOnAction(e-> {
            canvasController.processZoomInRequest();
        });
        zoomOutButton.setOnAction(e-> {
            canvasController.processZoomOutRequest();
        });
        gridBox.setOnAction(e-> {
            canvasController.processGrid(gridBox.isSelected());
        });
        snapBox.setOnAction(e-> {
            canvasController.processSnap(snapBox.isSelected());
        });
        resizeButton.setOnMouseDragged(e-> {
           canvasController.processCanvasMouseDrag(e.getX(), e.getY());
        });
        removeButton.setOnAction(e-> {
            canvasController.remove();
        });
        parentNameField.setOnKeyReleased(e-> {
            if (dataManager.getSelected() instanceof DesignerInterfaceRectangle) {
                ((DesignerInterfaceRectangle)dataManager.getSelected()).setEC(parentNameField.getText());
            } else {
                if (parentBox.getValue().equals("Extends")) {
                    
                    ((DesignerClassRectangle)dataManager.getSelected()).setEC(parentNameField.getText());
                } else {
                    ((DesignerClassRectangle)dataManager.getSelected()).setII(parentNameField.getText());
                }
            }
        });
    }

    public void clearFields() {
        classNameField.clear();
        packageGroup.getToggles().clear();
        packageField.clear();
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
        // NOTE THAT EACH CLASS SHOULD CORRESPOND TO
        // A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
        // CSS FILE
        toolContainerPane.getStyleClass().add(CLASS_MAIN_TOOLBARS);
        toolbarPane.getStyleClass().add(CLASS_SUB_TOOLBARS);
        viewPane.getStyleClass().add(CLASS_SUB_TOOLBARS);
        canvasPane.getStyleClass().add(CLASS_CANVAS);
        editClassElements.getStyleClass().add(CLASS_EDIT_MENU);

        tools.getStyleClass().add(CLASS_BUTTON);
        view.getStyleClass().add(CLASS_BUTTON);

        className.getStyleClass().add(CLASS_FONT);
        packageName.getStyleClass().add(CLASS_FONT);
        parentName.getStyleClass().add(CLASS_FONT);
        variables.getStyleClass().add(CLASS_FONT);
        methods.getStyleClass().add(CLASS_FONT);
        selectButton.getStyleClass().add(CLASS_BUTTON);
        resizeButton.getStyleClass().add(CLASS_BUTTON);
        addClassButton.getStyleClass().add(CLASS_BUTTON);
        addInterfaceButton.getStyleClass().add(CLASS_BUTTON);
        removeButton.getStyleClass().add(CLASS_BUTTON);
        undoButton.getStyleClass().add(CLASS_BUTTON);
        redoButton.getStyleClass().add(CLASS_BUTTON);
        zoomInButton.getStyleClass().add(CLASS_BUTTON);
        zoomOutButton.getStyleClass().add(CLASS_BUTTON);
        snapshot.getStyleClass().add(CLASS_BUTTON);
    }
    
    public void setUpParents() {
        if (dataManager.getSelected() instanceof DesignerInterfaceRectangle) {
            parentNameField.clear();
            ObservableList<String> options = 
                FXCollections.observableArrayList(
                    "Extends"
                );
            parentBox.setItems(options);
            parentBox.setOnAction(e->{
                if (parentBox.getValue() != null) {
                    if (parentBox.getValue().equals("Extends")) {
                        parentNameField.setText((dataManager.getSelected()).getEC());
                    }
                }
            });
        } else if (dataManager.getSelected() instanceof DesignerClassRectangle) {
            ObservableList<String> options = 
                FXCollections.observableArrayList(
                    "Extends",
                    "Implements"
                );
            parentBox.setItems(options);
            parentBox.setOnAction(e->{
                if (parentBox.getValue() != null) {
                    if (parentBox.getValue().equals("Extends")) {
                        parentNameField.setText(((DesignerClassRectangle)dataManager.getSelected()).getEC());
                    } else if (parentBox.getValue().equals("Implements")) {
                        parentNameField.setText(((DesignerClassRectangle)dataManager.getSelected()).getII());
                    }
                }
            });
        }
    }

    public void reloadCanvas() {
        ArrayList<DesignerData> data = dataManager.getData();
        setUpPackages();
        setUpParents();
        canvasPane.getChildren().clear();
        double zoomLevel = Math.pow(1.2, dataManager.getZoom());
        
        if (dataManager.getGridEnabled()) {
            for (int i=0; i<canvasPane.getWidth()/50; i++) {
                for (int j=0; j<canvasPane.getHeight()/50; j++) {
                    Line n = new Line();
                    n.setStartX(0);
                    n.setEndX(canvasPane.getWidth());
                    n.setStartY(j*50*zoomLevel);
                    n.setEndY(j*50*zoomLevel);
                    canvasPane.getChildren().add(n);
                }
                Line n = new Line();
                n.setStartY(0);
                n.setEndY(canvasPane.getWidth());
                n.setStartX(i*50*zoomLevel);
                n.setEndX(i*50*zoomLevel);
                canvasPane.getChildren().add(n);
            }
        }


        for (DesignerData n:data) {
            
            n.setX(n.getNormX()*zoomLevel);
            n.setY(n.getNormY()*zoomLevel);
            n.setWidth(n.getNormW()*zoomLevel);
            n.setHeight(n.getNormH()*zoomLevel);

            String textName = n.getClassName();


            Text name = new Text(n.getX() + 20*zoomLevel, n.getY() + 20*zoomLevel, textName);
            name.setWrappingWidth(n.getWidth() - (40*zoomLevel));
            name.setFont(new Font(name.getFont().getSize()*zoomLevel));
            Line line1 = new Line();
            if (n.getVariables().size()>0) {
                line1 = new Line();
                line1.setStartX(n.getX());
                line1.setEndX(n.getX() + n.getWidth());
                line1.setStartY(n.getY() + name.getLayoutBounds().getHeight() + 20*zoomLevel);
                line1.setEndY(n.getY() + name.getLayoutBounds().getHeight() + 20*zoomLevel);
            }

            textName += "\n";
            for (Variable var : n.getVariables()) {
                textName += "\n";
                textName += WorkspaceStringCreator.getVariableString(var);
            }
            name.setText(textName);
            Line line2 = new Line();
            if (n.getMethods().size()>0) {
                line2 = new Line();
                line2.setStartX(n.getX());
                line2.setEndX(n.getX() + n.getWidth());
                line2.setStartY(n.getY() + name.getLayoutBounds().getHeight() + 20*zoomLevel);
                line2.setEndY(n.getY() + name.getLayoutBounds().getHeight() + 20*zoomLevel);
            }
            textName += "\n";
            for (Method met : n.getMethods()) {
                textName += "\n";
                textName += WorkspaceStringCreator.getMethodString(met);
            }
            name.setText(textName);


            if (name.getLayoutBounds().getHeight() > n.getNormH() * zoomLevel) {
                n.setHeight((name.getLayoutBounds().getHeight() + 40*zoomLevel));
            }

            canvasPane.getChildren().addAll(n, name);
            if (!line1.equals(new Line())) {
                canvasPane.getChildren().addAll(line1);
            }
            if (!line2.equals(new Line())) {
                canvasPane.getChildren().addAll(line2);
            }
            checkForPoints(n);
        }
    }
    
    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
        try {
            
            reloadCanvas();
            
            if (packageGroup.getToggles().size() > 0) {
                removePackage.setDisable(false);
            } else {
                removePackage.setDisable(true);
            }
            
            if (dataManager.isSelectedNull()) {
                clearFields();
                
            } else if (dataManager.isNewSelected()){
                DesignerData currentSelected = dataManager.getSelected();
                classNameField.setText(currentSelected.getClassName());
                packageField.setText(currentSelected.getPackage());
                //packageNameField.setText(currentSelected.getPackageName());
                dataManager.setNewSelected(false);

                variablesPane.getChildren().clear();
                createVariableGraph();

                // Methods
                createMethodGraph();


            } else {
                variablesPane.getChildren().clear();
                createVariableGraph();

                // Mehods
                createMethodGraph();
            }
        } catch (Exception e) {

        }
    }
    
    private void createVariableGraph() {
        DesignerData currentSelected = dataManager.getSelected();
        reGridVariables();
        ArrayList<Variable> classVariables = currentSelected.getVariables();
        int currentRow = 1;
        for (Variable m : classVariables) {
            TextField mNameField = new TextField();
            mNameField.setMaxWidth(100);
            TextField mReturnField = new TextField();
            mReturnField.setMaxWidth(100);

            ComboBox<StateType> mStateField = new ComboBox<StateType>();
            mStateField.getItems().addAll(StateType.NONE, StateType.STATIC);
            mStateField.setMaxWidth(100);

            ComboBox<ProtectionType> mProtectionField = new ComboBox<ProtectionType>();
            mProtectionField.getItems().addAll(ProtectionType.PUBLIC,ProtectionType.PRIVATE,ProtectionType.PROTECTED,ProtectionType.NO_MODIFIER);
            mProtectionField.setMaxWidth(100);


            mNameField.setText(m.getName());
            mReturnField.setText(m.getType());
            mStateField.setValue(m.getState());
            mProtectionField.setValue(m.getProtection());
            Button removeVariable = new Button("Remove");
            removeVariable.setMinWidth(70);

            mNameField.setOnKeyReleased(e->{
                m.setName(mNameField.getText());
                reloadCanvas();
            });
            mReturnField.setOnKeyReleased(e->{
                m.setType(mReturnField.getText());
                reloadCanvas();
            });
            mStateField.setOnAction(e->{
                m.setState(mStateField.getValue());
                reloadCanvas();
            });
            mProtectionField.setOnAction(e->{
                m.setProtection(mProtectionField.getValue());
                reloadCanvas();
            });
            removeVariable.setOnAction(e->{
                classVariables.remove(m);
                reloadCanvas();
            });


            variablesPane.add(mNameField, 0, currentRow);
            variablesPane.add(mReturnField, 1, currentRow);
            variablesPane.add(mStateField, 2, currentRow);
            variablesPane.add(mProtectionField, 3, currentRow);
            variablesPane.add(removeVariable, 4, currentRow);
            currentRow++;
        }
        TextField mNameField = new TextField();
        TextField mReturnField = new TextField();
        mNameField.setMaxWidth(100);
        mReturnField.setMaxWidth(100);

        ComboBox<StateType> mStateField = new ComboBox<StateType>();
        mStateField.getItems().addAll(StateType.NONE, StateType.ABSTRACT, StateType.STATIC);
        mStateField.setMaxWidth(100);

        ComboBox<ProtectionType> mProtectionField = new ComboBox<ProtectionType>();
        mProtectionField.getItems().addAll(ProtectionType.PUBLIC,ProtectionType.PRIVATE,ProtectionType.PROTECTED,ProtectionType.NO_MODIFIER);
        mProtectionField.setMaxWidth(100);
        mNameField.setOnKeyReleased(e->{
            Variable m = new Variable();
            m.setName(mNameField.getText());
            currentSelected.getVariables().add(m);
            reloadWorkspace();
        });
        mReturnField.setOnKeyReleased(e->{
            Variable m = new Variable();
            m.setType(mReturnField.getText());
            currentSelected.getVariables().add(m);
            reloadWorkspace();
        });
        mStateField.setOnAction(e->{
            Variable m = new Variable();
            m.setState(mStateField.getValue());
            currentSelected.getVariables().add(m);
            reloadWorkspace();
        });
        mProtectionField.setOnAction(e->{
            Variable m = new Variable();
            m.setProtection(mProtectionField.getValue());
            currentSelected.getVariables().add(m);
            reloadWorkspace();
        });
        variablesPane.add(mNameField, 0, currentRow);
        variablesPane.add(mReturnField, 1, currentRow);
        variablesPane.add(mStateField, 2, currentRow);
        variablesPane.add(mProtectionField, 3, currentRow);
    }
    
    private void createMethodGraph() {
        DesignerData currentSelected = dataManager.getSelected();
        methodsPane.getChildren().clear();
        reGridMethods();
        ArrayList<Method> classMethods = currentSelected.getMethods();
        int currentMethodRow = 1;
        for (Method v : classMethods) {
            TextField vNameField = new TextField();
            TextField vReturnField = new TextField();
            vNameField.setMaxWidth(100);
            vReturnField.setMaxWidth(100);
            
            ComboBox<StateType> vStateField = new ComboBox<StateType>();
            vStateField.getItems().addAll(StateType.NONE, StateType.STATIC, StateType.ABSTRACT);
            vStateField.setValue(v.getState());
            vStateField.setMaxWidth(100);
            
            ComboBox<ProtectionType> vProtectionField = new ComboBox<ProtectionType>();
            vProtectionField.getItems().addAll(ProtectionType.PUBLIC,ProtectionType.PRIVATE,ProtectionType.PROTECTED,ProtectionType.NO_MODIFIER);
            vProtectionField.setValue(v.getProtection());
            vProtectionField.setMaxWidth(100);
            
            int argCounter = 4;
            for (Argument a:v.getArgs()) {
                TextField aTypeField = new TextField();
                aTypeField.setMaxWidth(100);
                aTypeField.setText(a.getType());
                aTypeField.setOnKeyReleased(e-> {
                    a.setType(aTypeField.getText());
                    reloadCanvas();
                });
                
                methodsPane.add(aTypeField, argCounter, currentMethodRow);
                argCounter++;
            }
            TextField aTypeField = new TextField();
            aTypeField.setMaxWidth(100);

            aTypeField.setOnKeyReleased(e-> {
                Argument a = new Argument();
                a.setType(aTypeField.getText());
                v.addArg(a);
                reloadCanvas();
            });
            
            Button removeVariable = new Button("Remove");
            removeVariable.setMinWidth(70);
            
            removeVariable.setOnAction(e->{
                classMethods.remove(v);
                reloadCanvas();
            });

            methodsPane.add(aTypeField, argCounter, currentMethodRow);
            
            
            vNameField.setText(v.getName());
            vReturnField.setText(v.getReturnType());

            vNameField.setOnKeyReleased(e->{
                v.setName(vNameField.getText());
                reloadCanvas();
            });
            vReturnField.setOnKeyReleased(e->{
                v.setReturnType(vReturnField.getText());
                reloadCanvas();
            });
            vStateField.setOnAction(e->{
                v.setState(vStateField.getValue());
                reloadCanvas();
            });
            vProtectionField.setOnAction(e->{
                v.setProtection(vProtectionField.getValue());
                reloadCanvas();
            });

            methodsPane.add(vNameField, 0, currentMethodRow);
            methodsPane.add(vReturnField, 1, currentMethodRow);
            methodsPane.add(vStateField, 2, currentMethodRow);
            methodsPane.add(vProtectionField, 3, currentMethodRow);
            methodsPane.add(removeVariable, argCounter+1, currentMethodRow);
            currentMethodRow++;
        }
        TextField vNameField = new TextField();
        TextField vReturnField = new TextField();
        vNameField.setMaxWidth(100);
        vReturnField.setMaxWidth(100);

        ComboBox<StateType> vStateField = new ComboBox<StateType>();
        vStateField.getItems().addAll(StateType.NONE, StateType.STATIC, StateType.ABSTRACT);
        vStateField.setMaxWidth(100);

        ComboBox<ProtectionType> vProtectionField = new ComboBox<ProtectionType>();
        vProtectionField.getItems().addAll(ProtectionType.PUBLIC,ProtectionType.PRIVATE,ProtectionType.PROTECTED,ProtectionType.NO_MODIFIER);
        vProtectionField.setMaxWidth(100);
        vNameField.setOnKeyReleased(e->{
            Method m = new Method();
            m.setName(vNameField.getText());
            currentSelected.getMethods().add(m);
            reloadWorkspace();
        });
        vReturnField.setOnKeyReleased(e->{
            Method m = new Method();
            m.setReturnType(vReturnField.getText());
            currentSelected.getMethods().add(m);
            reloadWorkspace();
        });
        vStateField.setOnAction(e->{
            Method m = new Method();
            m.setState(vStateField.getValue());
            currentSelected.getMethods().add(m);
            reloadWorkspace();
        });
        vProtectionField.setOnAction(e->{
            Method m = new Method();
            m.setProtection(vProtectionField.getValue());
            currentSelected.getMethods().add(m);
            reloadWorkspace();
        });
        int argCounter = 4;
        TextField aTypeField = new TextField();
        aTypeField.setMaxWidth(100);

        aTypeField.setOnKeyReleased(e-> {
            Method v = new Method();
            Argument a = new Argument();
            a.setType(aTypeField.getText());
            v.addArg(a);
            classMethods.add(v);
            reloadWorkspace();
        });

        methodsPane.add(aTypeField, argCounter, currentMethodRow);
        methodsPane.add(vNameField, 0, currentMethodRow);
        methodsPane.add(vReturnField, 1, currentMethodRow);
        methodsPane.add(vStateField, 2, currentMethodRow);
        methodsPane.add(vProtectionField, 3, currentMethodRow);
    }
    
    public void checkForPoints(DesignerData a) {
        if (a instanceof DesignerClassRectangle) {
            String str = ((DesignerClassRectangle) a).getII();
            String[] iArray = str.split(",");
            for (String n:iArray) {
                if (n!=null && !n.equals("")) {
                    System.out.println(n);
                    ArrayList<DesignerData> searchArray = dataManager.getGrid().getData();
                    for (DesignerData m:searchArray) {
                        if (m.getClassName().equals(n)) {
                            connect(a, m, 1);
                        }
                    }
                }
            }
        }
        String ec = a.getEC();
        if (ec!=null && !ec.equals("")) {
            ArrayList<DesignerData> searchArray = dataManager.getGrid().getData();
            for (DesignerData m:searchArray) {
                if (m.getClassName().equals(ec)) {
                    connect(a, m, 2);
                }
            }
        }
        
        ArrayList<DesignerData> searchArray2 = dataManager.getGrid().getData();
        for (DesignerData m:searchArray2) {
            for (Variable xyz:m.getVariables()) {
                if (!xyz.getType().equals("")) {
                    if (xyz.getType().equals(a.getClassName())) {
                        connect(m, a, 3);
                    }
                }
            }
        }
    }
    
    public void connect(DesignerData a, DesignerData b, int type) {
        System.out.println(type);
        Point2D a1 = new Point2D (a.getX(), a.getY());
        Point2D a2 = new Point2D (a.getX()+a.getWidth(), a.getY());
        Point2D a3 = new Point2D (a.getX(), a.getY()+a.getHeight());
        Point2D a4 = new Point2D (a.getX()+a.getWidth(), a.getY()+a.getHeight());
        
        Point2D[] aArray = new Point2D[4];
        aArray[0] = a1;
        aArray[1] = a2;
        aArray[2] = a3;
        aArray[3] = a4;
        
        Point2D b1 = new Point2D (b.getX(), b.getY());
        Point2D b2 = new Point2D (b.getX()+b.getWidth(), b.getY());
        Point2D b3 = new Point2D (b.getX(), b.getY()+b.getHeight());
        Point2D b4 = new Point2D (b.getX()+b.getWidth(), b.getY()+b.getHeight());
        
        Point2D[] bArray = new Point2D[4];
        bArray[0] = b1;
        bArray[1] = b2;
        bArray[2] = b3;
        bArray[3] = b4;
        
        Point2D closestA = aArray[0];
        Point2D closestB = bArray[0];
        double distance = aArray[0].distance(bArray[0]);
        for (Point2D a1234 : aArray) {
            for (Point2D b1234: bArray) {
                if (distance > a1234.distance(b1234)) {
                    closestA = a1234;
                    closestB = b1234;
                    distance = a1234.distance(b1234);
                }
            }
        }
        
        Point2D newA = new Point2D(0, 0);
        Point2D newB = new Point2D(0, 0);
        if (closestA.equals(a1) || closestA.equals(a2)) {
            newA = new Point2D((a1.getX()+a2.getX())/2, a1.getY());
        } else if (closestA.equals(a3) || closestA.equals(a4)) {
            newA = new Point2D((a3.getX()+a4.getX())/2, a3.getY());
        }
        if (closestB.equals(b1) || closestB.equals(b2)) {
            newB = new Point2D((b1.getX()+b2.getX())/2, b1.getY());
        } else if (closestB.equals(b3) || closestB.equals(b4)) {
            newB = new Point2D((b3.getX()+b4.getX())/2, b3.getY());
        }
        
        Line line1 = new Line(newA.getX(), newA.getY(), newA.getX(), (newA.getY()+newB.getY())/2);
        Line line2 = new Line(newA.getX(), (newA.getY()+newB.getY())/2, newB.getX(), (newA.getY()+newB.getY())/2);
        if (type == 3) {
            Line line3 = new Line(newB.getX(), (newA.getY()+newB.getY())/2, newB.getX(), newB.getY());
            Line line4 = new Line(0,0,0,0);
            Line line5 = new Line(0,0,0,0);
            if (newB.getY() - newA.getY() > 0) {
                line4 = new Line(newB.getX(), newB.getY(), newB.getX()-10, newB.getY()-10);
                line5 = new Line(newB.getX(), newB.getY(), newB.getX()+10, newB.getY()-10);
            } else {
                line4 = new Line(newB.getX(), newB.getY(), newB.getX()-10, newB.getY()+10);
                line5 = new Line(newB.getX(), newB.getY(), newB.getX()+10, newB.getY()+10);
            }
            canvasPane.getChildren().addAll(line1, line2, line3, line4, line5);
        } else if (type == 2) {
            Line line3 = new Line(0,0,0,0);
            Line line4 = new Line(0,0,0,0);
            Line line5 = new Line(0,0,0,0);
            Line line6 = new Line(0,0,0,0);
            Line line7 = new Line(0,0,0,0);
            if (newB.getY() - newA.getY() > 0) {
                line3 = new Line(newB.getX(), (newA.getY()+newB.getY())/2, newB.getX(), newB.getY()-20);
                line4 = new Line(newB.getX(), newB.getY(), newB.getX()-5, newB.getY()-10);
                line5 = new Line(newB.getX(), newB.getY(), newB.getX()+5, newB.getY()-10);
                line6 = new Line(newB.getX()-5, newB.getY()-10, newB.getX(), newB.getY()-20);
                line7 = new Line(newB.getX()+5, newB.getY()-10, newB.getX(), newB.getY()-20);
            } else {
                line3 = new Line(newB.getX(), (newA.getY()+newB.getY())/2, newB.getX(), newB.getY()+20);
                line4 = new Line(newB.getX(), newB.getY(), newB.getX()+5, newB.getY()+10);
                line5 = new Line(newB.getX(), newB.getY(), newB.getX()-5, newB.getY()+10);
                line6 = new Line(newB.getX()-5, newB.getY()+10, newB.getX(), newB.getY()+20);
                line7 = new Line(newB.getX()+5, newB.getY()+10, newB.getX(), newB.getY()+20);
            }
            canvasPane.getChildren().addAll(line1, line2, line3, line4, line5, line6, line7);
        } else if (type == 1) {
            Line line3 = new Line(0,0,0,0);
            Line line4 = new Line(0,0,0,0);
            Line line5 = new Line(0,0,0,0);
            Line line6 = new Line(0,0,0,0);
            if (newB.getY() - newA.getY() > 0) {
                line3 = new Line(newB.getX(), (newA.getY()+newB.getY())/2, newB.getX(), newB.getY()-10);
                line4 = new Line(newB.getX(), newB.getY(), newB.getX()-10, newB.getY()-10);
                line5 = new Line(newB.getX(), newB.getY(), newB.getX()+10, newB.getY()-10);
                line6 = new Line(newB.getX()+10, newB.getY()-10, newB.getX()-10, newB.getY()-10);
            } else {
                line3 = new Line(newB.getX(), (newA.getY()+newB.getY())/2, newB.getX(), newB.getY()+10);
                line4 = new Line(newB.getX(), newB.getY(), newB.getX()-10, newB.getY()+10);
                line5 = new Line(newB.getX(), newB.getY(), newB.getX()+10, newB.getY()+10);
                line6 = new Line(newB.getX()-10, newB.getY()+10, newB.getX()+10, newB.getY()+10);
            }
            canvasPane.getChildren().addAll(line1, line2, line3, line4, line5, line6);
        }
        
    }
}
