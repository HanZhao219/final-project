/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.gui;

import jcd.data.Argument;
import jcd.data.Enums.ProtectionType;
import jcd.data.Enums.StateType;
import jcd.data.Method;
import jcd.data.Variable;

/**
 *
 * @author Han
 */
public class WorkspaceStringCreator {
    public static String getVariableString(Variable v) {
        String output = "";
        if (v.getProtection() == ProtectionType.PUBLIC) {
            output += "+";
        } else if (v.getProtection() == ProtectionType.PROTECTED) {
            output += "#";
        } else if (v.getProtection() == ProtectionType.PRIVATE) {
            output += "-";
        } else if (v.getProtection() == ProtectionType.NO_MODIFIER) {
            output += "~";
        }
        if (v.getState() == StateType.STATIC) {
            output += "$";
        }
        output = output + v.getName() + " : " + v.getType();
        return output;
    }
    
    public static String getMethodString(Method v) {
        String output = "";
        if (v.getProtection() == ProtectionType.PUBLIC) {
            output += "+";
        } else if (v.getProtection() == ProtectionType.PROTECTED) {
            output += "#";
        } else if (v.getProtection() == ProtectionType.PRIVATE) {
            output += "-";
        } else if (v.getProtection() == ProtectionType.NO_MODIFIER) {
            output += "~";
        }
        if (v.getState() == StateType.STATIC) {
            output += "$";
        }
        output += v.getName() + "(";
        int counter = 1;
        for (Argument a : v.getArgs()) {
            output = output + "arg" + counter + " : " + a.getType();
        }
        output += ")";
        output = output + " : " + v.getReturnType();
        if (v.getState() == StateType.ABSTRACT) {
            output += " {abstract}";
        }
        return output;
    }
}
